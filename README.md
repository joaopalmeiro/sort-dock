# sort-dock

## Development

```bash
pyenv install && pyenv versions
```

```bash
pip install pipenv && pipenv --version
```

```bash
pipenv install --dev --skip-lock
```

```bash
pipenv run ruff check --verbose .
```

```bash
pipenv run ruff check --fix --verbose . && pipenv run ruff format --verbose .
```

```bash
pipenv run python 01.py
```
