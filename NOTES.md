# Notes

- https://sh.readthedocs.io/en/latest/usage.html
- https://macos-defaults.com/dock/orientation.html
- https://www.real-world-systems.com/docs/defaults.1.html
- https://stackoverflow.com/a/5038929
- https://en.wikipedia.org/wiki/Property_list
- https://docs.python.org/3.10/library/plistlib.html
- https://pypi.org/project/userdefaults3/ + https://github.com/ongyx/userdefaults3
- https://docs.python.org/3/library/tempfile.html
- https://github.com/SethMMorton/fastnumbers
- https://apple.stackexchange.com/questions/319796/how-do-i-move-finder-icon-location-in-the-dock
- https://superuser.com/a/206950
- https://github.com/dnicolson/binary-plist
- https://athena.trixology.com/index.php?topic=2606.0
- https://stackoverflow.com/questions/69122322/applescript-remove-specific-items-from-plist-dock-using-the-provided-persisten
- https://ss64.com/osx/killall.html
- https://ss64.com/osx/pkill.html
- https://developer.apple.com/library/archive/documentation/System/Conceptual/ManPages_iPhoneOS/man3/signal.3.html
- https://ss64.com/osx/pkill.html
- https://stackoverflow.com/questions/36420022/how-can-i-compare-two-ordered-lists-in-python
- https://apple.fandom.com/wiki/Dock_Extra

## Commands

```bash
pipenv install --skip-lock sh==2.0.6 && pipenv install --dev --skip-lock ruff==0.1.6
```

```bash
defaults export com.apple.dock -
```

```bash
defaults help
```

```bash
defaults export com.apple.dock - > dock.plist
```

```bash
pipenv run python 01.py > dock.output.plist
```

```bash
defaults import com.apple.dock dock.plist && killall Dock
```

```bash
cd ~/Library/Preferences/ByHost/
```

```bash
cd ~/Library/Preferences/ && code com.apple.dock.plist
```

```bash
defaults read com.apple.dock
```

```bash
ps -A | grep Dock
```

```bash
pgrep com.apple.dock.extra
```
