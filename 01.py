import plistlib

from sh import defaults, killall


def get_app_comparison_key(app):
    key = app["tile-data"]["file-label"]
    # print(key)

    return key


def print_apps(apps, prefix):
    app_names = [get_app_comparison_key(app) for app in apps]

    print(f"{prefix}: {', '.join(app_names)}")


if __name__ == "__main__":
    # https://github.com/python/cpython/blob/v3.10.13/Lib/plistlib.py
    # https://github.com/python/cpython/blob/v3.10.13/Lib/plistlib.py#L332
    # https://github.com/python/cpython/blob/v3.10.13/Lib/plistlib.py#L318
    # .removeprefix(plistlib.PLISTHEADER)
    # .removeprefix('<plist version="1.0">')
    # .removesuffix("</plist>\n")

    # print(pgrep("Dock"))

    # https://sh.readthedocs.io/en/latest/sections/passing_arguments.html
    current_dock_config = defaults("export", "com.apple.dock", "-")
    # print(current_dock_config, type(current_dock_config))

    # https://docs.python.org/3/library/stdtypes.html#str.encode
    parsed_dock_config = plistlib.loads(current_dock_config.encode())
    # print(parsed_dock_config["largesize"])
    # print(parsed_dock_config["persistent-apps"])
    # `<class 'list'> <class 'dict'>`:
    # print(
    #     type(parsed_dock_config["persistent-apps"]),
    #     type(parsed_dock_config["persistent-apps"][0]),
    # )

    print_apps(parsed_dock_config["persistent-apps"], "From")

    new_sorted_apps = sorted(
        parsed_dock_config["persistent-apps"], key=get_app_comparison_key, reverse=False
    )
    parsed_dock_config["persistent-apps"] = new_sorted_apps

    print_apps(new_sorted_apps, "To")

    # `new_dock_config` does not need to be a string:
    new_dock_config = plistlib.dumps(
        parsed_dock_config, fmt=plistlib.FMT_XML, sort_keys=False, skipkeys=False
    )
    # print(new_dock_config)

    # https://sh.readthedocs.io/en/latest/sections/stdin.html
    defaults("import", "com.apple.dock", "-", _in=new_dock_config)
    killall("Dock")

    print("Done!")
